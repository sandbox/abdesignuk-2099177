core = 7.x
api = 2
projects[] = drupal

; Modules
; Contrib
projects[] = admin
projects[] = ctools
projects[] = ckeditor
projects[] = context
projects[] = date
projects[] = ds
projects[] = entityreference
projects[] = entity
projects[] = features
projects[] = inline_entity_form
projects[] = jquery_update
projects[] = module_filter
projects[] = panels
projects[] = panels_everywhere
projects[] = panelizer
projects[] = rules
projects[] = token
projects[] = views
projects[] = views_slideshow
projects[] = webform

; CREAM
projects[] = timetable


